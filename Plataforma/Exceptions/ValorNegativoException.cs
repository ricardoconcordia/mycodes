﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Exceptions
{
    /// <summary>
    /// Classe para tratar exceção de vendas com valores negativos
    /// </summary>
    public class ValorNegativoException : ApplicationException
    {
        private static void DividirPorZero(int x)
        {
            if (x != 0)
            {
                var y = 2 / x;
            }
            else
            {
                //Lançando a exceção para ser tratada onde o método foi chamado
                throw new DivideByZeroException();
            }
        }

        public static void Escrever(string[] linhas)
        {
            //Pega informações do assembly corrente desse programa em execução usando a Reflection
            var x = System.Reflection.Assembly.GetExecutingAssembly();
            //Pega o diretório onde irei salvar o arquivo
            var path = System.IO.Path.GetDirectoryName(x.Location);
            //Para cada posição do vetor será escrita uma nova linha no arquivo
            System.IO.File.WriteAllLines(path + "\text.pad", linhas);
            
            System.IO.StreamWriter sw = null;
            try
            {
                //abre um canal de comunicação por meio de stream com o local do arquivo
                sw = new System.IO.StreamWriter(path + "\text.pad");
                //Para cada posição do meu vetor, escreva no arquivo
                foreach (var linha in linhas)
                {
                    sw.WriteLine(linha);
                }
            }
            catch (Exception e)
            {
                
                throw;
            }
            finally
            {
                if(sw != null)
                {
                    //fecha o fluxo de stream de dados
                    sw.Close();
                    //tira da memória
                    sw.Dispose();
                }
            }

            

            //No caso de leitura de um arquivo
            System.IO.StreamReader sr = null;
            string textoQueQueroLer = "";
            try
            {
                //abre um canal de comunicação por meio de stream com o local do arquivo
                sr = new System.IO.StreamReader(path + "\text.pad");
                //Enquanto o fluxo nao tiver acabado
                while (!sr.EndOfStream)
                {
                    textoQueQueroLer = textoQueQueroLer + sr.ReadLine();
                }
                
            }
            catch (Exception e)
            {

                throw;
            }
            finally
            {
                if (sr != null)
                {
                    //fecha o fluxo de stream de dados
                    sr.Close();
                    //tira da memória
                    sr.Dispose();
                }
            }
        }
    }
}
