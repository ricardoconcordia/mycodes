﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma
{
        public class Produto        
    {
        //Atributos estáticos nao mudam de estado e não precisam de instanciação
        public long ProdutoId { get; set; }
        public string Nome { get; set; }
        public decimal PrecoVenda { get; set; }
        public decimal PrecoCusto { get; set; }
        //Todo tipo no c# que você quer que receba null basta coloca '?' na frente do tipo da variável
        public DateTime? DataValidade { get; set; }

        //Produto perecível é aquele que tem data de validade
        private bool perecivel;

        public bool Perecivel
        {
            get {
                //Como a propriedade aceita nulo é sempre necessário verificar se ela tem valor
                if (this.DataValidade.HasValue)
                    return true;
                
                return false;
            }
            //O valor só poderá ser atribuido dentro da própria classe
            private set { perecivel = value; }
        }

        static void Main(string[] args)
        {
            //Deve ser construído no construtor
            //Funciona como um dicionário, usando par valor, onde a primeira string é o que se procura e a segunda string
            //todos os seus significados - OBS: não é uma coleção, somente um par de valores
            KeyValuePair<string, string> y = new KeyValuePair<string, string>("Computador", "Não sei");

            //Já é uma coleção de par valores
            //Muito utilizado para fazer DE-PARA, para buscas extremamente rápidas
            Dictionary<string, string> yCollection = new Dictionary<string, string>();

            //Em métodos que exigem um parâmetro de saída, ou seja, onde a saída do método será armazenada
            string outParametro;
            //TryGetValue retorna true ou falso - Se retornou true ele armazena o valor na variável out
            yCollection.TryGetValue("Computador", out outParametro);

        }
    }

    /// <summary>
    /// Classe genérica para automóvel passando o tipo de automóvel
    /// onde T so pode ser do tipo de automóvel que herdam do tipo T
    /// </summary>
    /// <typeparam name="T">Sedan, SUV, Hatch</typeparam>
    public class Automovel<T> where T:Automovel<T>
    {
        public int NumeroDeRodas { get; set; }
        public double Peso { get; set; }
        public double Largura { get; set; }
        public double Comprimento { get; set; }
        public int NumeroPortas { get; set; }
        public double Velocidade { get; set; }

        public  void Acelerar(double KMH)
        {

        }

        public void Frear(double KMH)
        {

        }

        public void AlterarVelocidade(double KMH)
        {

        }

        public void Parar(T tipoVeiculo)
        {
            if(tipoVeiculo is Sedan)
            {
                while (tipoVeiculo.Velocidade > 0)
                {
                    tipoVeiculo.Frear(40.0);
                }
            }
            else if (tipoVeiculo is Vanzinha)
            {
                //variável de vanzinha para fazer um cast
                Vanzinha vanzinha = tipoVeiculo as Vanzinha;
                while (tipoVeiculo.Velocidade > 0)
                {
                    //Operador ternário if
                    tipoVeiculo.Frear(vanzinha.Escolar ? 10.0 : 20.0);
                }
            }
        }

        private async Task<object> BuscarClienteAsync(int id)
        {
            string comando = "SELECT Nome FROM Clientes WHERE Id = @Id";

            using (SqlConnection sc = new SqlConnection("caminho do bd"))
            {
                var tarefaDeAberturaDeConexao = sc.OpenAsync();
                SqlConnection
            }
        }
    }

    /// <summary>
    /// Classe abstrata Sedan que herda de automóvel que é genérica
    /// Se vc quiser usar as coisas legais da classe automóvel , vc precisa ser um tipo de automóvel
    /// e instanciar a classe automóvel passando o seu tipo como parâmetro
    /// </summary>
    public abstract class Sedan : Automovel<Sedan>
    {
        public double TamanhoDoBumBum { get; set; }

        public Sedan()
        {

        }

        static void Main(string[] args)
        {
            Automovel<Sedan> t = new Automovel<Sedan>();
        }
    }

    public abstract class Vanzinha : Automovel<Vanzinha>
    {
        public bool Escolar { get; set; }
    }

}
