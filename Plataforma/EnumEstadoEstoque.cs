﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma
{
    class Enumeradores
    {
        enum EstadoEstoque
        {
            //Seta o número 1 para começar com ele.
            Cheio = 1,
            Vazio,
            PrecisaRepor
        }

        void Teste()
        {
            EstadoEstoque ee = EstadoEstoque.Vazio;

            //fazendo um cast
            //dizendo para o compilador que "ee" é para ser entendido como inteiro
            int eeInt = (int)ee;

            switch (ee)
            {
                case EstadoEstoque.Cheio:
                    break;
                case EstadoEstoque.Vazio:
                    break;
                case EstadoEstoque.PrecisaRepor:
                    break;
                default:
                    break;
            }
        }
    }
}
