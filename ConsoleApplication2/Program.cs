﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        private static List<string> Estacionamento = new List<string>();

        static void Main(string[] args)
        {
            Thread t1 = new Thread(EntradaVeiculo);
            t1.IsBackground = true;
            Thread t2 = new Thread(EntradaVeiculo);
            t2.IsBackground = true;

            t1.Start();
            t2.Start();
        }

        [MTAThread]
        private static void EntradaVeiculo()
        {
            while(true)
            {
                //Bloqueia o recurso para que somente cada uma delas insira de uma vez..
                lock (Estacionamento)
                {
                    Estacionamento.Add("Palio");
                }
                
                //Dorme em um tempo aleatório de 1 a 10 minutos
                Thread.Sleep(TimeSpan.FromMinutes(new Random().Next(1, 10)));
            }
        }
    }
}
